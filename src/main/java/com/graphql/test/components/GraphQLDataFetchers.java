package com.graphql.test.components;

import com.google.common.collect.ImmutableMap;
import graphql.schema.DataFetcher;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
public class GraphQLDataFetchers { // this is used to resolve queries

    // some fake data while we do not have a database connection
    private static final List<Map> tpiRecords = Arrays.asList(
            ImmutableMap.builder()
                    .put("id", "007")
                    .put("sku", "96196")
                    .put("storeNumber", "7633")
                    .put("skuName", "VMS DT EPA FISH OIL 1000MG 180")
                    .put("storeName", "7633 / GOUDA")
                    .put("categoryName", "VHMS")
                    .put("inventoryDateTime", "2021-12-03T17:02:22.293918")
                    .put("deliveryDate", "2021-10-19")
                    .put("packSize", "1")
                    .put("safetyStock", "3")
                    .put("onHandQuantity", "6")
                    .put("orderQuantity", "5")
                    .put("orderCalDate", "2021-10-15")
                    .put("cost", "4.31")
                    .put("prediction", "2")
                    .put("predictionWeekEnding", "2021-10-31")
                    .put("retailSellingPrice", "27.99")
                    .build()
    );


    // datafetcher for the TPI record query, uses fake data but it could reach into a database to return that instead
    public DataFetcher getTPIRecordByPredictionWeekEndingDataFetcher() {
        return dataFetchingEnvironment -> {
            String predictionWeekEnding = dataFetchingEnvironment.getArgument("predictionWeekEnding");
            return tpiRecords
                    .stream()
                    .filter(record -> record.get("predictionWeekEnding").equals(predictionWeekEnding))
                    .findFirst()
                    .orElse(null);
        };
    }

    // field resolver for the above query 
    // when get the above query is run, this will run additional logic to resolve the storeNumber field
    // It is very flexible and can even reach into different databases
    public DataFetcher storeNumberFieldResolver() {
        return dataFetchingEnvironment -> {
            return 10;
        };
    }

}
